@Acquisition
Feature: To validate Interested in Selling And Leasing Land form, Business a Development form(covering 7 user story P11981-260, P11981-871, P11981-878, P11981-244, P11981-899, P11981-900, P11981-307)
@Feb16
  Scenario: Validate Interested in Selling And Leasing Land form
    Given User should Launch the MCW application
    When User should scroll down and click on Acquisitions and Development link on footer
    And User should click on Store Development Real Estate category
    And User should clicking the Interested in Selling And Leasing Land hyperlink
    And User should fill the value as Business Name "Msw", Website "http://sample.com", Name First "James", Name Last "Robert", Phone Number "1234567898", Email Address "test@gmail.com", City "Tuxon", State "TX", Zip "23223", Lot Size "2"
    Then User should verify the success message of form submission
    Then User should verify the mandatory fields
    Then User should verify the error message by without filled any data in form
    Then User Should verify the error message by filled value as alpha numeric

  Scenario: Validate Acquisitions, Store Development Real Estate, Development and Construction header
    Given User should Launch the MCW application
    When User should scroll down and click on Acquisitions and Development link on footer
    Then Verify the Acquisitions, Store Development Real Estate, Development and Construction header
    Then Verify the each section whether having CTA action

  Scenario: Validate Become a Development Partner form
    Given User should Launch the MCW application
    When User should scroll down and click on Acquisitions and Development link on footer
    And User should click on Development and Construction category
    And User should clicking Become a Development Partner hyperlink
    And User should fill the value as Business Name "Msw", Name First "James", Name Last "Robert", Phone Number "1234567898", Email Address "test@gmail.com", Website "http://sample.com", Industry "Engineer", Have you built "No", Company Annual Rev "121",Construction Mgment System "Test", state "Alaska", Comments " test"
    Then User should verify the success message of Become a Development form submission
    Then User should verify the mandatory fields for BD form
    Then User should verify the error message by without filled any data in BD form
    Then User Should verify the email field by filled invalid data
