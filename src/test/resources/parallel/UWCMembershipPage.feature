@UWCMemberTerms
Feature: Validate user should able to view the Unlimited Wash Club Membership Terms(Covering user story P11981-461)

  Scenario Outline: Verify the Unlimited Wash Club Membership Terms
    Given User should Launch the MCW application
    When User should scroll down and click on UWC Membership Terms link on footer
    Then User should be able to see the following sections "<Term1>","<Term2>","<Term3>","<Term4>","<Term5>","<Term6>","<Term7>","<Term8>","<Term9>"
      Examples:
      | Term1   | Term2 | Term3    | Term4                         | Term5                              | Term6                  | Term7                                                  | Term8                                                                     | Term9         |
      | PURPOSE | SCOPE | OVERVIEW | UWC Membership Plans and Fees | Billing, Cancellation, and Refunds | UWC Membership Account | Disclaimers of Warranties and Limitations on Liability | Arbitration Agreement, Waiver of Trial by Jury and Waiver of Class Action | Miscellaneous |

