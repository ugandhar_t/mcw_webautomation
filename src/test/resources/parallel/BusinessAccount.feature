@BusinessAcc
Feature: Validate user should house account form or wash pass pricing form(covering 3 user story P11981-478, P11981-479, P11981-480)

  Background:
    Given User should Launch the MCW application
    When As a Guest user is on the business account page
    And As a Guest user is click on Read more link from Wash pass section to enable Wash pass pricing button
    And As a Guest user navigate to wash pricing form

  Scenario Outline: Verify the Wash Pass Pricing form fields titles, error messages
    Then Verify the following fields in wash pricing form "<ContactNameField>","<CompNameField>","<PhoneField>","<EmailField>","<AddressField>","<FleetInfoField>","<TypeOfBusinessField>","<NumebrOfVehiinFleetField>","<RegionYourCompServicField>","<OtherField>",
    Then Verify the mandatory fields for Contact Name, Company Name, Phone Number, Email Address, Address
    And User fill the invalid value for email as "test", state "33", zipcode as "text" and get relevant error message respective to fields

    Examples:
      | ContactNameField | CompNameField | PhoneField   | EmailField    | AddressField | FleetInfoField    | TypeOfBusinessField | NumebrOfVehiinFleetField    | RegionYourCompServicField    | OtherField |
      | Contact Name     | Company Name  | Phone Number | Email Address | Address      | Fleet Information | Type of Business    | Number of vehicles in fleet | Region your company services | Other:     |

  Scenario Outline: Verify the Wash Pass Pricing form with valid message
    When User should enter the value "<ContactNameFirstField>","<ContactNameLastField>","<CompNameField>","<PhoneField>","<EmailField>","<StreetAddrField>","<AddrLineField2>","<StateField>","<ZipField>"
    And User should see the success message of form submission
    And User should close the success message modal window
    And As a Guest user is on the business account page
    Examples:
      | ContactNameFirstField | ContactNameLastField | CompNameField | PhoneField   | EmailField            | StreetAddrField | AddrLineField2 | StateField | ZipField | FleetInfoField | TypeOfBusinessField | NumebrOfVehiinFleetField | RegionYourCompServicField | OtherField |
      | James                 | Robert               | Mcw           | 713-395-1111 | robert35236@gmail.com | Raleigh St      | 3333           | TX         | 77021    | Houston        | Test                | 3                        | Houston                   | Comments   |