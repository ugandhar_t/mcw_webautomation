package parallel;

import com.pages.HomePage;
import com.qa.factory.DriverFactory;
import com.qa.util.ConfigReader;
import io.cucumber.java.en.Given;

import java.io.IOException;

public class HomePageSteps extends ConfigReader {

	HomePage hp = new HomePage();
	//private HomePage hp;

	@Given("User should Launch the MCW application")
	public void user_should_Launch_the_MCW_application() throws IOException {
		DriverFactory.getDriver().get(getData("url"));
	}

	@Given("verify the buywash menu")
	public void user_is_on_accounts_page() {
		hp.getTit();
		System.out.println("Test");
	}

	}
