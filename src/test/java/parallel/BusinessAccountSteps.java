package parallel;

import com.pages.BusinessAccountPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BusinessAccountSteps {
    BusinessAccountPage bAP=new BusinessAccountPage();
    @When("As a Guest user is on the business account page")
    public void asAGuestUserIsOnTheBusinessAccountPage() {
        bAP.asAGuestUserIsOnTheBusinessAccountPage();
    }

    @And("As a Guest user is click on Read more link from Wash pass section to enable Wash pass pricing button")
    public void asAGuestUserIsClickOnReadMoreLinkFromWashPassSectionToEnableWashPassPricingButton() {
        bAP.clickReadMoreFromWashPass();
    }

    @And("As a Guest user navigate to wash pricing form")
    public void asAGuestUserNavigateToWashPricingForm() {
        bAP.asAGuestUserNavigateToWashPricingForm();
    }

    @Then("Verify the following fields in wash pricing form {string},{string},{string},{string},{string},{string},{string},{string},{string},{string},")
    public void verifyTheFollowingFieldsInWashPricingForm(String ContactNameField, String CompNameField, String PhoneField, String EmailField, String AddressField, String FleetInfoField, String TypeOfBusinessField, String NumebrOfVehiinFleetField, String RegionYourCompServicField, String OtherField) {
    bAP.verifyTheFollowingFieldsInWashPricingForm(ContactNameField,CompNameField,PhoneField,EmailField,AddressField,FleetInfoField,TypeOfBusinessField,NumebrOfVehiinFleetField,RegionYourCompServicField,OtherField);
    }

    @Then("Verify the mandatory fields for Contact Name, Company Name, Phone Number, Email Address, Address")
    public void verifyTheMandatoryFieldsForContactNameCompanyNamePhoneNumberEmailAddressAddress() {
        bAP.verifyTheMandatoryFieldsforWashPassForm();
    }

    @And("User should enter the value {string},{string},{string},{string},{string},{string},{string},{string},{string}")
    public void userShouldEnterTheValue(String ContactNameFirstField,String ContactNameLastField, String CompNameField, String PhoneField, String EmailField, String StreetAddrField,String AddrLineField2,String StateField, String ZipField) {
        bAP.userShouldEnterTheValue(ContactNameFirstField,ContactNameLastField,CompNameField,PhoneField,EmailField,StreetAddrField,AddrLineField2,StateField,ZipField);
    }


    @And("User should see the success message of form submission")
    public void userShouldSeeTheSuccessMessageOfFormSubmission() {
        bAP.userShouldSeeTheSuccessMessageOfFormSubmission();
    }

    @And("User should close the success message modal window")
    public void userShouldCloseTheSuccessMessageModalWindow() {
        bAP.userShouldCloseTheSuccessMessageModalWindow();
    }

    @And("User fill the invalid value for email as {string}, state {string}, zipcode as {string} and get relevant error message respective to fields")
    public void userFillTheInvalidValueForEmailAsStateZipcodeAsAndGetRelevantErrorMessageRespectiveToFields(String email, String state, String zip) {
    bAP.userGetRelevantErrorMessageforFields(email,state,zip);
    }
}
