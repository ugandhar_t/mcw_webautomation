package parallel;

import com.pages.UWCMembershipPage;
import com.qa.util.ConfigReader;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UWCMembershipSteps {
    UWCMembershipPage uwcMem=new UWCMembershipPage();
    @When("User should scroll down and click on UWC Membership Terms link on footer")
    public void userShouldScrollDownAndClickOnUWCMembershipTermsLinkOnFooter() throws Exception {
        uwcMem.clickUWSMemberTerms();
    }

    @Then("User should be able to see the following sections {string},{string},{string},{string},{string},{string},{string},{string},{string}")
    public void userShouldBeAbleToSeeTheFollowingSections(String Term1, String Term2, String Term3, String Term4, String Term5, String Term6, String Term7, String Term8, String Term9) {
        uwcMem.membershipMenu(Term1,Term2,Term3,Term4,Term5,Term6,Term7,Term8,Term9);
    }
}
