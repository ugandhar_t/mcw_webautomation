package parallel;

import com.pages.AcquisitionDevelopmentPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AcquisitionsDevelopmentSteps {
    AcquisitionDevelopmentPage aDP=new AcquisitionDevelopmentPage();
    @When("User should scroll down and click on Acquisitions and Development link on footer")
    public void userShouldScrollDownAndClickOnAcquisitionsAndDevelopmentLinkOnFooter() throws Exception {
        aDP.userShouldScrollDownAndClickOnAcquisitionsAndDevelopmentLinkOnFooter();

    }
    @Then("Verify the Acquisitions, Store Development Real Estate, Development and Construction header")
    public void verifyTheAcquisitionsStoreDevelopmentRealEstateDevelopmentAndConstructionHeader() {
        aDP.verifyTheAcquisitionsStoreDevelopmentRealEstateDevelopmentAndConstructionHeader();
    }
    @Then("Verify the each section whether having CTA action")
    public void verifyTheEachSectionWhetherHavingCTAAction() {
        aDP.verifyTheEachSectionWhetherHavingCTAAction();
    }

    @And("User should click on Store Development Real Estate category")
    public void userShouldClickOnStoreDevelopmentRealEstateCategory() {
        aDP.setStoreDevRealEstateReadmore();
    }

    @And("User should clicking the Interested in Selling And Leasing Land hyperlink")
    public void userShouldClickingTheInterestedInSellingAndLeasingLandHyperlink() {
        aDP.userShouldClickingTheInterestedInSellingLeasingLandHyperlink();
    }

    @And("User should fill the value as Business Name {string}, Website {string}, Name First {string}, Name Last {string}, Phone Number {string}, Email Address {string}, City {string}, State {string}, Zip {string}, Lot Size {string}")
    public void userShouldFillTheValueAsBusinessNameWebsiteNameFirstNameLastPhoneNumberEmailAddressAvailablePropertyStateZipLotSize(String businessName, String website, String nameFirst, String nameLast, String phoneNumber, String emailAddress, String city, String state, String zip, String lotSize) {
        aDP.userShouldFillFormInInterestedInSellingLeasingWithValidValue(businessName,website,nameFirst,nameLast,phoneNumber,emailAddress,city,state,zip,lotSize);
    }

    @And("User should verify the success message of form submission")
    public void userShouldVerifyTheSuccessMessageOfFormSubmission() {
        aDP.userShouldVerifyTheSuccessMessageOfFormSubmission();
    }
    @Then("User should verify the mandatory fields")
    public void userShouldVerifyTheMandatoryFields() {
        aDP.userShouldVerifyTheMandatoryFields();
    }

    @Then("User should verify the error message by without filled any data in form")
    public void userShouldVerifyTheErrorMessageByWithoutFilledAnyDataInForm() {
        aDP.userShouldVerifyTheErrorMessageByWithoutFilledAnyDataInForm();
    }

    @Then("User Should verify the error message by filled value as alpha numeric")
    public void userShouldVerifyTheErrorMessageByFilledValueAsAlphaNumeric() {
        aDP.userShouldVerifyTheErrorMessageByFilledValueAsAlphaNumeric();
    }


}
