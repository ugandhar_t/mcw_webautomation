package com.pages;

import com.qa.factory.DriverFactory;
import com.qa.util.ElementUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AcquisitionDevelopmentPage extends ElementUtil {
	//public static RemoteWebDriver driver;

	public AcquisitionDevelopmentPage() {

		PageFactory.initElements(DriverFactory.getDriver(), this);
	}

	@FindBy(xpath = "//h2[@class='card-title']")
	public static List<WebElement> acquisionAndStoreDevAndDevConstructionTitle;
	@FindBy(xpath = "//button[text()='Read More! ']")
	public static WebElement acquisitionsReadmoreBtn;
	@FindBy(xpath = "//button[text()='Interested in Selling']")
	public static WebElement acquisionInterestedInSellBtn;

	/*Interested in Selling/Leasing Land*/
	@FindBy(xpath = "//a[text()='Acquisitions & Development']")
	public static WebElement AcquisiAndDevelopmentLink;
	@FindBy(xpath = "//button[text()='Read More!']")
	public static WebElement storeDevRealEstateReadmore;
	@FindBy(xpath = "//button[text()='Interested in Selling/Leasing Land']")
	public static WebElement interestInSellAndLeasingLand;
	@FindBy(xpath = "//input[contains(@id,'input_7')]")
	public static List<WebElement> interestInSellFormAllField;
	@FindBy(xpath = "//input[@type='submit'][@id='gform_submit_button_7']")
	public static WebElement interestInSelSubmitBtn;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::h1[text()='Thanks for contacting us']")
	public static WebElement sucessMsgOfIntAndSellingFormSub;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::div[contains(text(),'There was a problem with your submission. Errors have been')]")
	public static WebElement errorMsgInIntAndSellingForm;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::input[@id='input_7_4_3']")
	public static WebElement cityFieldValue;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::input[@id='input_7_4_4']")
	public static WebElement stateFieldValue;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::input[@id='input_7_4_5']")
	public static WebElement zipCodeFieldValue;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::div[contains(text(),'City, State should only contain alphabets! and Zip Code')]")
	public static WebElement cityStateZipErrorMsg;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::input[@id='input_7_3']")
	public static WebElement emaillAddFieldValue;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::div[text()='Please enter a valid email address.']")
	public static WebElement emailValidationErrorMsg;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::input[@id='input_7_5']")
	public static WebElement phoneNumbFieldValue;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::input[@id='input_7_1']")
	public static WebElement businessNameFildVal;
	@FindBy(xpath = "//div[@id='leasingform']/descendant::div/button[text()='Close']")
	public static WebElement sucessCloseBtnModalWind;

	/*Become a development form*/
	@FindBy(xpath = "//button[text()='Read More!   ']")
	public static WebElement devAndConsReadMore;
	@FindBy(xpath = "//button[text()='Become a Development Partner']")
	public static WebElement becomeDevPartBtn;


	/*Acquisitions, Store Development Real Estate and Development Construction header verification*/
	public void verifyTheAcquisitionsStoreDevelopmentRealEstateDevelopmentAndConstructionHeader(){
		for (WebElement header: acquisionAndStoreDevAndDevConstructionTitle) {
			if(header.getText().contains("Acquisitions")) {
				Assert.assertTrue(getText(header).contains("Acquisitions"));
			}
			else if (header.getText().contains("Store Development")){
				Assert.assertTrue(getText(header).contains("Store Development Real Estate"));
			}
			else{
				Assert.assertTrue(getText(header).contains("Development and Construction"));
			}
		}
	}

	public void verifyTheEachSectionWhetherHavingCTAAction(){
		ClickOnWebElement(acquisitionsReadmoreBtn);
		waitFor(2000);
		Assert.assertTrue(getText(acquisionInterestedInSellBtn).contains("Interested in Selling"));
		ClickOnWebElement(storeDevRealEstateReadmore);
		waitFor(2000);
		Assert.assertTrue(getText(interestInSellAndLeasingLand).contains("Interested in Selling/Leasing Land"));
		ClickOnWebElement(devAndConsReadMore);
		waitFor(2000);
		Assert.assertTrue(getText(becomeDevPartBtn).contains("Become a Development Partner"));

	}

	/*Web Element actions method*/

	public void userShouldScrollDownAndClickOnAcquisitionsAndDevelopmentLinkOnFooter() throws Exception {
		waitFor(2000);
		scrollToElement(AcquisiAndDevelopmentLink);
		ClickOnWebElement(AcquisiAndDevelopmentLink);
		Assert.assertEquals("User is on AcquisionAndDevelopmentLink Page","Acquisitions & Development | Mister Car Wash",getPageTitle());
	}

	public void setStoreDevRealEstateReadmore(){
		ClickOnWebElement(storeDevRealEstateReadmore);
	}

	public void userShouldClickingTheInterestedInSellingLeasingLandHyperlink(){
		ClickOnWebElement(interestInSellAndLeasingLand);
	}

	public void userShouldFillFormInInterestedInSellingLeasingWithValidValue(String businessName, String website, String nameFirst, String nameLast, String phoneNumber, String emailAddress, String city, String state, String zip, String lotSize){
		HashMap<Integer,String> mp=new HashMap<>();
		mp.put(0,businessName);
		mp.put(1,website);
		mp.put(2,nameFirst);
		mp.put(3,nameLast);
		mp.put(4,phoneNumber);
		mp.put(5,emailAddress);
		mp.put(6,city);
		mp.put(7,state);
		mp.put(8,zip);
		mp.put(10,lotSize);

		for(int i=0;i<=mp.size();i++){
			if(mp.containsKey(i))
			SendKeysOnWebElement(interestInSellFormAllField.get(i),mp.get(i));
		}
		ClickOnWebElement(interestInSelSubmitBtn);
	}

	public void userShouldVerifyTheSuccessMessageOfFormSubmission(){
		waitFor(2000);
		Assert.assertTrue(getText(sucessMsgOfIntAndSellingFormSub).contains("Thanks for contacting us"));
		ClickOnWebElement(sucessCloseBtnModalWind);
		Assert.assertEquals("User is on AcquisionAndDevelopmentLink Page","Acquisitions & Development | Mister Car Wash",getPageTitle());
	}

	public void userShouldVerifyTheMandatoryFields(){
		waitFor(1000);
		setStoreDevRealEstateReadmore();
		userShouldClickingTheInterestedInSellingLeasingLandHyperlink();
		Assert.assertTrue(getAttributeValue(businessNameFildVal,"aria-required"),true);
		Assert.assertTrue(getAttributeValue(phoneNumbFieldValue,"aria-required"),true);
		Assert.assertTrue(getAttributeValue(emaillAddFieldValue,"aria-required"),true);
		Assert.assertTrue(getAttributeValue(cityFieldValue,"aria-required"),true);
		Assert.assertTrue(getAttributeValue(stateFieldValue,"aria-required"),true);
		Assert.assertTrue(getAttributeValue(zipCodeFieldValue,"aria-required"),true);
	}

	public void userShouldVerifyTheErrorMessageByWithoutFilledAnyDataInForm(){
		ClickOnWebElement(interestInSelSubmitBtn);
		waitFor(1000);
		Assert.assertTrue(getText(errorMsgInIntAndSellingForm).contains("There was a problem with your submission"));
	}

	public void userShouldVerifyTheErrorMessageByFilledValueAsAlphaNumeric(){
		SendKeysOnWebElement(cityFieldValue,"Houston123");
		SendKeysOnWebElement(stateFieldValue,"TX123");
		SendKeysOnWebElement(zipCodeFieldValue,"TX123");
		SendKeysOnWebElement(emaillAddFieldValue,"Test@");
		ClickOnWebElement(interestInSelSubmitBtn);
		waitFor(1000);
		Assert.assertTrue(getText(cityStateZipErrorMsg).contains("City, State should only contain alphabets! and Zip Code should only contain numbers!"));
		Assert.assertTrue(getText(emailValidationErrorMsg).contains("Please enter a valid email address"));
	}


}
