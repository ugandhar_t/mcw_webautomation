package com.pages;

import com.qa.factory.DriverFactory;
import com.qa.util.ElementUtil;
import net.bytebuddy.implementation.bytecode.assign.TypeCasting;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BusinessAccountPage extends ElementUtil {

	public BusinessAccountPage() {

		PageFactory.initElements(DriverFactory.getDriver(), this);
	}
	/*Web Elements of BusinessAccount page*/
	@FindBy(xpath = "//a[text()='Services']")
	public static WebElement servicesMenu;
	@FindBy(xpath = "//a[text()='Business Accounts']")
	public static WebElement businessAccLink;
	@FindBy(xpath = "//h2[text()='Wash Pass']/parent::div//child::button[text()='Read More!']")
	public static WebElement washPassReadMore;
	@FindBy(xpath = "//a[text()='Wash Pass Pricing']")
	public static WebElement washPricingBtn;
	@FindBy(xpath = "//label[text()='Contact Name']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement contactNameField;
	@FindBy(xpath = "//label[text()='Contact Name']/ancestor::li[contains(@id,'field_13')]/label/span[@class='gfield_required']")
	public static WebElement contactNameMandatory;
	@FindBy(xpath = "//label[text()='Company Name']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement companyNameField;
	@FindBy(xpath = "//label[text()='Company Name']/ancestor::li[contains(@id,'field_13')]/label/span[@class='gfield_required']")
	public static WebElement companyNameMandatory;
	@FindBy(xpath = "//label[text()='Phone Number']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement phoneNumField;
	@FindBy(xpath = "//label[text()='Phone Number']/ancestor::li[contains(@id,'field_13')]/label/span[@class='gfield_required']")
	public static WebElement phoneNumMandatory;
	@FindBy(xpath = "//label[text()='Email Address']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement emailAddrField;
	@FindBy(xpath = "//label[text()='Email Address']/ancestor::li[contains(@id,'field_13')]/label/span[@class='gfield_required']")
	public static WebElement emailAddrMandatory;
	@FindBy(xpath = "//input[@id='input_13_3']")
	public static WebElement emailAddrValue;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']//div[text()='Please enter a valid email address.']")
	public static WebElement emailInvalidErrorMsg;
	@FindBy(xpath = "//label[text()='Address']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement addressField;
	@FindBy(xpath = "//label[text()='Address']/ancestor::li[contains(@id,'field_13')]/label/span[@class='gfield_required']")
	public static WebElement addressMandatory;
	@FindBy(xpath = "//h2[text()='Fleet Information']/parent::li[contains(@id,'field_13')]/h2")
	public static WebElement fleetInfoField;
	@FindBy(xpath = "//label[text()='Type of Business']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement typeOfBusiField;
	@FindBy(xpath = "//label[text()='Number of vehicles in fleet']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement numOfVehInFleetField;
	@FindBy(xpath = "//label[text()='Region your company services']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement regYourCompServField;
	@FindBy(xpath = "//label[text()='Other:']/ancestor::li[contains(@id,'field_13')]/label")
	public static WebElement other;
	@FindBy(xpath = "//input[@type='submit'][@id='gform_submit_button_13']")
	public static WebElement submitBtn;
	@FindBy(xpath = "//div[@class='form-section']/descendant::div[@class='validation_error']")
	public static WebElement invalidErrorMsg;
	@FindBy(xpath = "//span[@class='name_first'][@id='input_13_1_3_container']/input[@type='text']")
	public static WebElement contactNameFirstValue;
	@FindBy(xpath = "//input[@id='input_13_2']")
	public static WebElement companyNameValue;
	@FindBy(xpath = "//span[@class='name_last'][@id='input_13_1_6_container']/input[@type='text']")
	public static WebElement contactNameLastValue;
	@FindBy(xpath = "//input[@id='input_13_4']")
	public static WebElement phoneNumValue;
	@FindBy(xpath = "//input[@id='input_13_5_1']")
	public static WebElement addrValue;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']/descendant::input[@name='input_5.2']")
	public static WebElement addresLine2Value;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']/descendant::input[@name='input_5.4']")
	public static WebElement stateValue;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']/descendant::input[@name='input_5.5']")
	public static WebElement zipCodeValue;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']//div[contains(text(),'State should only contain alphabets')]")
	public static WebElement stateAndZipErrorMsg;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']/descendant::h1[text()='Thanks for contacting us']")
	public static WebElement verifySuccessMsg;
	@FindBy(xpath = "//div[@id='houseaccountnewblock_614c6ef4094a8_1']/descendant::button[text()='Close']")
	public static WebElement closeSuccessPopUp;


	/*Web Element actions method*/

	public void asAGuestUserIsOnTheBusinessAccountPage(){
		mouseOver(servicesMenu);
		mouseOver(businessAccLink);
		ClickOnWebElement(businessAccLink);
		Assert.assertEquals("Business Account Page","Business Accounts | Mister Car Wash",getPageTitle());
	}

	public void clickReadMoreFromWashPass(){
		ClickOnWebElement(washPassReadMore);

	}

	public void asAGuestUserNavigateToWashPricingForm(){
		ClickOnWebElement(washPricingBtn);
	}
	public void verifyTheFollowingFieldsInWashPricingForm(String ContactNameField, String CompNameField, String PhoneField, String EmailField, String AddressField, String FleetInfoField, String TypeOfBusinessField, String NumebrOfVehiinFleetField, String RegionYourCompServicField, String OtherField){
		Map<String,String> map=new LinkedHashMap<>();
		map.put(getText(contactNameField),ContactNameField);
		map.put(getText(companyNameField),CompNameField);
		map.put(getText(phoneNumField),PhoneField);
		map.put(getText(emailAddrField),EmailField);
		map.put(getText(addressField),AddressField);
		map.put(getText(fleetInfoField),FleetInfoField);
		map.put(getText(typeOfBusiField),TypeOfBusinessField);
		map.put(getText(numOfVehInFleetField),NumebrOfVehiinFleetField);
		map.put(getText(regYourCompServField),RegionYourCompServicField);
		map.put(getText(other),OtherField);

		for(Map.Entry<String,String> value: map.entrySet()){
			String s=value.getKey();
			String actValue=s.replace("*","").trim();
			Assert.assertEquals("Field Found",value.getValue(),actValue);
		}
	}
	public void verifyTheMandatoryFieldsforWashPassForm(){
		Assert.assertEquals(true,IsDisplayedWebElement(contactNameMandatory));
		Assert.assertEquals(true,IsDisplayedWebElement(companyNameMandatory));
		Assert.assertEquals(true,IsDisplayedWebElement(phoneNumMandatory));
		Assert.assertEquals(true,IsDisplayedWebElement(emailAddrMandatory));
		Assert.assertEquals(true,IsDisplayedWebElement(addressMandatory));
	}
	public void userGetRelevantErrorMessageforFields(String email, String state, String zip){
		SendKeysOnWebElement(emailAddrValue,email);
		SendKeysOnWebElement(stateValue,state);
		SendKeysOnWebElement(zipCodeValue,zip);
		ClickOnWebElement(submitBtn);
		waitFor(2000);
		Assert.assertTrue(getText(invalidErrorMsg).contains("There was a problem with your submission"));
		Assert.assertTrue(getText(emailInvalidErrorMsg).contains("Please enter a valid email address"));
		Assert.assertTrue(getText(stateAndZipErrorMsg).contains("State should only contain alphabets! and Zip Code should only contain numbers"));

	}
	public void userShouldEnterTheValue(String ContactNameFirstField,String ContactNameLastField, String CompNameField, String PhoneField, String EmailField, String StreetAddrField,String AddrLineField2,String StateField, String ZipField){
		waitFor(1000);
		SendKeysOnWebElement(contactNameFirstValue,ContactNameFirstField);
		SendKeysOnWebElement(contactNameLastValue,ContactNameLastField);
		SendKeysOnWebElement(companyNameValue,CompNameField);
		SendKeysOnWebElement(phoneNumValue,PhoneField);
		SendKeysOnWebElement(emailAddrValue,EmailField);
		SendKeysOnWebElement(addrValue,StreetAddrField);
		SendKeysOnWebElement(addresLine2Value,AddrLineField2);
		SendKeysOnWebElement(stateValue,StateField);
		SendKeysOnWebElement(zipCodeValue,ZipField);
		ClickOnWebElement(submitBtn);
		waitFor(4000);
	}

	public void userShouldSeeTheSuccessMessageOfFormSubmission(){
		waitFor(1000);
		containsString(getText(verifySuccessMsg),"Thanks");
	}

	public void userShouldCloseTheSuccessMessageModalWindow(){
		ClickOnWebElement(closeSuccessPopUp);
	}

}
