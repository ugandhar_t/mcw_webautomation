package com.pages;

import com.qa.factory.DriverFactory;
import com.qa.util.ElementUtil;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class UWCMembershipPage extends ElementUtil {
	//public static RemoteWebDriver driver;

	public UWCMembershipPage() {

		PageFactory.initElements(DriverFactory.getDriver(), this);
	}

	@FindBy(xpath = "//a[text()='Membership Terms']")
	public static WebElement UWCMemTermsLink;

	@FindBy(xpath = "//nav[@class='tabs']/descendant::a/child::img/parent::a")
	public static List<WebElement> MembershipMenuLink;

	/*Web Element actions method*/

	public static void clickUWSMemberTerms() throws Exception {
		waitFor(2000);
		scrollToElement(UWCMemTermsLink);
		ClickOnWebElement(UWCMemTermsLink);
		Assert.assertEquals("Valid","Member Terms | Mister Car Wash",getPageTitle());
	}

	public List<String> membershipMenu(String Term1, String Term2, String Term3, String Term4, String Term5, String Term6, String Term7, String Term8, String Term9){
		List<String> menu= new ArrayList<>();

		for (int i=0; i<MembershipMenuLink.size();i++){
			String menuInd=MembershipMenuLink.get(i).getText();
			menu.add(menuInd);
			if(menuInd.contains(Term1)||menuInd.contains(Term2)||menuInd.contains(Term3)||menuInd.contains(Term4)||menuInd.contains(Term5)||menuInd.contains(Term6)||menuInd.contains(Term7)||menuInd.contains(Term8)||menuInd.contains(Term9)){
				Assert.assertTrue("Terms Found",true);
			}
			else {
				Assert.assertFalse("Terms Not found",false);
			}
		}
		System.out.println("Menu"+menu);
		return menu;
	}
}
