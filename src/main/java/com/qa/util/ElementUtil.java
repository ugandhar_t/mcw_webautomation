package com.qa.util;

import com.qa.factory.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.*;

import java.awt.*;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/*This class is used to handle all the web element related actions*/

public class ElementUtil {
    //public static RemoteWebDriver driver;
    public static int timeOut = 0;
    public static int elementWaitInSeconds = 0;
    public int retryattempts = 1;
    public Select select;
    public static WebDriverWait exwait = null;




    /*This method is used to get the text from web element*/
    public static String getText(WebElement element) {
        return element.getText();
    }

    /*This method is used to set implicit wait*/

    public void setImplicit(int timeOut) {
        DriverFactory.getDriver().manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    /*This method is used to get the url from webpage */
    public void getUrl(String url) {
        DriverFactory.getDriver().get(url);
        setImplicit(timeOut);
    }

    /*This method is used to WebDriverWait*/
    public WebDriverWait webDriverWait() {
        return new WebDriverWait(DriverFactory.getDriver(), elementWaitInSeconds);
    }

    /*This method is used to WebDriverWait with Visibility of element method*/
    public void waitVisibilityOfElement(WebElement element) {
        webDriverWait().until(ExpectedConditions.visibilityOf(element));
    }

    /*This method is used to WebDriverWait with element to be clickable method*/
    public void waitElementToBeClickable(WebElement element) {
        webDriverWait().until(ExpectedConditions.elementToBeClickable(element));
    }

    /*This method is used to mouseover operation*/
    public void mouseOver(WebElement element) {
        waitVisibilityOfElement(element);
        new Actions(DriverFactory.getDriver()).moveToElement(element).build().perform();
    }

    public static void doubleClick(WebElement element) {
        waitFor(1000);
        Actions act = new Actions(DriverFactory.getDriver());
        act.moveToElement(element).doubleClick().perform();
        waitFor(1500);
    }

    public static void copyText() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyPress(java.awt.event.KeyEvent.VK_C);
        robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyRelease(java.awt.event.KeyEvent.VK_C);
        waitFor(1000);
    }

    public static void pasteText() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyPress(java.awt.event.KeyEvent.VK_V);
        robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyRelease(java.awt.event.KeyEvent.VK_V);
    }

    /*This method used for switch to parent window*/
    public void switchToParentWindow() {
        Set<String> winHandles = DriverFactory.getDriver().getWindowHandles();
        for (String wHandle : winHandles) {
            DriverFactory.getDriver().switchTo().window(wHandle);
            break;
        }
    }

    /*This method used for switch to Last window*/
    public void switchToLastWindow() {
        Set<String> winHandles = DriverFactory.getDriver().getWindowHandles();
        for (String wHandle : winHandles) {
            DriverFactory.getDriver().switchTo().window(wHandle);
        }
    }

    /*This method used for scrolling the page*/
    public static void scrollToElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    public void highLighterMethod(WebDriver driver, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: ; border: 2px solid blue;');", element);
    }

    public static void waitFor(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public boolean staleRetryingElementIsDisplayed(WebElement element) {
        boolean result = false;
        int attempts = 0;
        while (attempts < retryattempts) {
            try {
                waitVisibilityOfElement(element);
                element.isDisplayed();
                result = true;
                break;
            } catch (StaleElementReferenceException e) {
                waitFor(500);
            }
            attempts++;
        }
        return result;
    }

    public boolean isElementDisplayed(WebElement element) {
        boolean flag;
        try {
            setImplicit(10);
            scrollToElement(element);
            waitVisibilityOfElement(element);
            highLighterMethod(DriverFactory.getDriver(), element);
            flag = element.isDisplayed();
        } catch (StaleElementReferenceException stale) {
            flag = staleRetryingElementIsDisplayed(element);
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }
    public String getAttributeValue(WebElement element, String attributeName) {
        waitVisibilityOfElement(element);
        return element.getAttribute(attributeName);
    }

    public Select selectDropdown(WebElement element) {

        select = new Select(element);
        return select;
    }

    public void selectByIndex(WebElement element, int index) {

        selectDropdown(element).selectByIndex(index);
    }

    public void SelectByValue(WebElement element, String value) {
        waitFor(1000);
        selectDropdown(element).selectByValue(value);
    }

    public void SelectByVisibleText(WebElement element, String text) {
        waitFor(1000);
        selectDropdown(element).selectByVisibleText(text);
    }

    public static String getPageTitle() {

        return DriverFactory.getDriver().getTitle();
    }



    public void clearTextInTheTextField(WebElement element) {

        waitVisibilityOfElement(element);
        element.clear();
    }

    public static void ClickOnWebElement(WebElement element) {
        exwait = new WebDriverWait(DriverFactory.getDriver(), 60);
        WebElement webelement = exwait.until(ExpectedConditions.visibilityOf(element));
        JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getDriver();
        js.executeScript("arguments[0].setAttribute('style', 'background: lightskyblue; border: 2px solid red;');",
                webelement);
        waitFor(2000);
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", webelement, "");
        webelement.click();
    }

    public static void SendKeysOnWebElement(WebElement element, String Value) {
        exwait = new WebDriverWait(DriverFactory.getDriver(), 60);
        WebElement webelement = exwait.until(ExpectedConditions.visibilityOf(element));
        webelement.clear();
        webelement.sendKeys(Value);
    }

    public static void sendKeys(WebElement element, String value) {
        exwait = new WebDriverWait(DriverFactory.getDriver(), 60);
        WebElement webelement = exwait.until(ExpectedConditions.visibilityOf(element));
        webelement.sendKeys(Keys.ENTER);
        waitFor(3000);
        webelement.clear();
        webelement.sendKeys(value);
    }
    public static boolean IsDisplayedWebElement(WebElement element) {
        exwait = new WebDriverWait(DriverFactory.getDriver(), 60);
        WebElement webelement = exwait.until(ExpectedConditions.visibilityOf(element));
        return webelement.isDisplayed();
    }

    public static void scrollDown(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,600)");
    }

    public static void scrollUp(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,-250)");
    }

    public static void scrollDownTillLast(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,7200)");
    }
    public void imageVisible(WebElement image) {
        Boolean ImagePresent = (Boolean) ((JavascriptExecutor) DriverFactory.getDriver()).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", image);
        if (!ImagePresent) {
            System.out.println("Image not displayed.");
        } else {
            System.out.println("Image displayed.");
        }
    }

    public void bgColor(WebElement color) {
        String beforehover = color.getCssValue("color");
        Actions builder = new Actions(DriverFactory.getDriver());
        builder.moveToElement(color).build().perform();
        String afterhover = color.getCssValue("color");
        if (beforehover != afterhover) {
            System.out.println("Color change while hovering");
        } else {
            System.out.println("Color not changing");
        }
    }

    public static void select_Option_In_DropDown_ByVisibleText(
            WebElement element, String sVisibleTextOptionToSelect) {
        try {
            Select select = new Select(element);
            select.selectByVisibleText(sVisibleTextOptionToSelect);
        } catch (NoSuchElementException e) {
            System.out.println("Option value not find in dropdown");

        }
    }

    public static void verifyTitle(String expectedTitle) {

        if (!(DriverFactory.getDriver().getTitle().replaceAll("[^a-zA-Z0-9]", "").toLowerCase().contains(expectedTitle.replaceAll("[^a-zA-Z0-9]","").toLowerCase()))) {

            Assert.fail(" Actual Page Title: " + DriverFactory.getDriver().getTitle().replaceAll("[^a-zA-Z0-9]", "").toLowerCase() +"\n"+" Expected Title:  " + expectedTitle.toLowerCase());

        } else {
            //Reporter.addStepLog(" Actual Page Title: " + driver.getTitle() + " Expected Title: " + expectedTitle);
        }

    }

    public void clickwithScroll(WebElement scrollToElement, WebElement clickElement) {

        waitFor(1000);
        scrollToElement(scrollToElement);
        waitVisibilityOfElement(clickElement);
        waitElementToBeClickable(clickElement);
        clickElement.click();
    }
    public static void containsString(String str1, String str2) {
        waitFor(1000);
        if (!(str1.trim().contains(str2.trim()))) {
            Assert.fail("Actual Text: " + str1 + " Expected contains Text: " + str2);
        } else {
            //Assert.assertEquals("Actual Text: " + str1," Expected contains Text: " + str2);
            Assert.assertTrue(str1.contains(str2));

        }
    }




}
